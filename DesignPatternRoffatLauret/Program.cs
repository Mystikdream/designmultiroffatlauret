﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class Program
    {
        static void Main(string[] args)
        {

            //LoggerDebug test = new LoggerDebug();
            //test.Message = "plop";

            //Console.WriteLine(test.Message);
            //Console.ReadLine();

            Director myDir = new Director();
            ILoggerBuilder monBuild = null;

            monBuild = ConcretBuilderInfo.UniqueInstance;
            myDir.construct(monBuild);
            monBuild.UnLogger.display("Ceci est une information de la classe program dans la fonction mainhyh");


            monBuild = ConcretBuilderDebug.UniqueInstance;
            myDir.construct(monBuild);
            monBuild.UnLogger.display("Ceci est un message de debug de la classe program dans la fonction mainhyh");


            monBuild = ConcretBuilderError.UniqueInstance;
            myDir.construct(monBuild);
            monBuild.UnLogger.display("Ceci est un message d'erreur de la classe program dans la fonction mainhyh");
            Console.ReadKey();
        }
    }
}
