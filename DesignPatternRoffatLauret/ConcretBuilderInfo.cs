﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class ConcretBuilderInfo : ConcretBuilder
    {
        private static ConcretBuilderInfo uniqueInstance;

        public static ConcretBuilderInfo UniqueInstance
        {
            get
            {
                if (uniqueInstance == null)
                {
                    uniqueInstance = new ConcretBuilderInfo();
                }
                return uniqueInstance;
            }
        }

        public ConcretBuilderInfo()
        {
         
        }

        public override void Build()
        {

            UnLogger = new LoggerInfo();
        }
    }
}
