﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class LogXmlFormat
    {

        public LogXmlFormat(string message, string name, string classe)
        {

            string newMess = makeXml(message, name, classe);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("./logs/errors.txt"))
            {
                file.WriteLine(newMess);
            }

        }

        public string makeXml(string message, string name, string classe)
        {

            string oldLogs = "";
            string[] text = System.IO.File.ReadAllLines("./logs/errors.txt");

            foreach (string line in text)
            {
                if (line.Contains("< record "))
                {
                    oldLogs += line + Environment.NewLine;
                }
            }

            string monXml = "<? xml version = \"1.0\" encoding = \"ISO-8859-1\" ?>" + Environment.NewLine +
                "   < log >" + Environment.NewLine + oldLogs +
                "       < record class= \"" + classe + "\" method=\"" + name + "\" message=\"" + message + "\"/>" + Environment.NewLine +
                "   </log>";               ;

            return monXml;
        }

    }
}
