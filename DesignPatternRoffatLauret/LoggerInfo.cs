﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class LoggerInfo : Logger
    {
        public override void display(string message, [System.Runtime.CompilerServices.CallerMemberName] string name = "", [System.Runtime.CompilerServices.CallerFilePath] string classe = "")
        {
            Visiteur.visit(this, message, name, classe);
        }
    }
}
