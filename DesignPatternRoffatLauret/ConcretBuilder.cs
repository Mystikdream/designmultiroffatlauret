﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    abstract class ConcretBuilder : ILoggerBuilder
    {

        //public Logger UnLogger { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        private Logger unLoger;
        public Logger UnLogger
        {
            get
            {
                return this.unLoger;
            }
            set
            {
                this.unLoger = value;
            }
        }

        public abstract void Build();
    }
}
