﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    interface ILoggerBuilder
    {
        Logger UnLogger { get; set;}
        void Build();
    }
}
