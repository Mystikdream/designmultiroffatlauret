﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class Director
    {
        public void construct (ILoggerBuilder concretBuilder)
        {
            concretBuilder.Build();
        }
    }
}
