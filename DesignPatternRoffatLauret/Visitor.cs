﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class Visitor
    {
        public void visit(LoggerInfo o, string message, string name, string classe)
        {
            string messToDisp = classe.Substring(classe.LastIndexOf("\\") + 1, classe.LastIndexOf(".") - classe.LastIndexOf("\\")) + name + ": " + message;
            new LogRowFormat(messToDisp);

        }
        public void visit(LoggerDebug o, string message, string name, string classe)
        {
            string messToDisp = "DEBUG : " + classe.Substring(classe.LastIndexOf("\\") + 1, classe.LastIndexOf(".") - classe.LastIndexOf("\\")) + name + ": " + message;
            new LogRowFormat(messToDisp);
        }
        public void visit(LoggerError o, string message, string name, string classe)
        {
            string messToDisp = classe.Substring(classe.LastIndexOf("\\") + 1, classe.LastIndexOf(".") - classe.LastIndexOf("\\")) + name + ": " + message;
            new LogRowFormat(messToDisp);
            new LogXmlFormat(message, name, classe.Substring(classe.LastIndexOf("\\") + 1, classe.LastIndexOf(".") - classe.LastIndexOf("\\") - 1));
        }
    }
}
