﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class ConcretBuilderDebug : ConcretBuilder
    {
        private static ConcretBuilderDebug uniqueInstance;

        public static ConcretBuilderDebug UniqueInstance
        {
            get
            {
                if (uniqueInstance == null)
                {
                    uniqueInstance = new ConcretBuilderDebug();
                }
                return uniqueInstance;
            }
        }

        public ConcretBuilderDebug()
        {

        }

        public override void Build()
        {

            UnLogger = new LoggerDebug();
        }
    }
}
