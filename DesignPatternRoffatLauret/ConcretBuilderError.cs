﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    class ConcretBuilderError : ConcretBuilder
    {
        private static ConcretBuilderError uniqueInstance;

        public static ConcretBuilderError UniqueInstance
        {
            get
            {
                if (uniqueInstance == null)
                {
                    uniqueInstance = new ConcretBuilderError();
                }
                return uniqueInstance;
            }
        }

        public ConcretBuilderError()
        {

        }

        public override void Build()
        {

            UnLogger = new LoggerError();
        }
    }
}
