﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternRoffatLauret
{
    abstract class Logger
    {
        
        private Visitor visiteur = new Visitor();

        public Visitor Visiteur
            {
            get {
            return visiteur;
            }
    }
        
        public abstract void display(string message, [System.Runtime.CompilerServices.CallerMemberName] string name = "", [System.Runtime.CompilerServices.CallerFilePath] string classe = "");
        //public void display()
        //{
        //    visiteur.visit(this);
        //}

    }
}
